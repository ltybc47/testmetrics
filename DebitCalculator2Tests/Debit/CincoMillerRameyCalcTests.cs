﻿using DebitCalculator2.Debit;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace DebitCalculator2Tests.Debit
{
    [TestFixture()]
    public class CincoMillerRameyCalcTests
    {
        private List<Variable> variables;
        private CincoMillerRameyCalc calc;

        [SetUp]
        public void Setup()
        {
            variables = GenerateVariables();
            calc = new CincoMillerRameyCalc();
        }
        // Из за неизвесной lg не проходит 
        [Test()]
        public void CincoMillerRameyCalcTest_GetS_return0dot001()
        {
            calc.AddListVariables(variables);
            var result = calc.S;
            Console.WriteLine(result);

            Assert.IsTrue(Math.Abs(Math.Abs(result) - 0.001570246) < 0.1);
        }

        // по калькулятору тоже результат не сходится.
        [Test()]
        public void CincoMillerRameyCalcTest_GetHd_return47434()
        {
            calc.AddListVariables(variables);
            var result = calc.hd;
            Console.WriteLine(result);

            Assert.IsTrue(Math.Abs(Math.Abs(result) - 47434.16) < 0.1);
        }

        [Test()]
        public void CincoMillerRameyCalcTest_GetJd_return0dot1249()
        {
            calc.AddListVariables(variables);
            var result = calc.Jd;
            Console.WriteLine(result);

            Assert.IsTrue(Math.Abs(Math.Abs(result) - 47434.16) < 0.1);
        }

        [Test()]
        public void CincoMillerRameyCalcTest_GetQzh_return122dot142942()
        {
            calc.AddListVariables(variables);
            var result = calc.Qzh;
            Console.WriteLine(result);

            Assert.IsTrue(Math.Abs(Math.Abs(result) - 122.142942) < 0.1);
        }

        private List<Variable> GenerateVariables()
        {
            return new List<Variable>() {
                Variables.GetVariable("h", 18),
                Variables.GetVariable("a", 0.001),
                Variables.GetVariable("Pпл", 150),
                Variables.GetVariable("Pзаб", 50),
                Variables.GetVariable("Kv", 2),
                Variables.GetVariable("Kh", 1000),
                Variables.GetVariable("m", 100),
                Variables.GetVariable("bж", 1),
                Variables.GetVariable("re", 300),
                Variables.GetVariable("rw", 0.1),
                Variables.GetVariable("lg", 1), // что это и откуда непонятно.
                Variables.GetVariable("fi", 0.523599) };
        }
    }
}