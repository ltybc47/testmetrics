﻿using DebitCalculator2.Interfaces;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using Moq;

namespace DebitCalculator2Tests
{
    [TestFixture]
    public class TestClass
    {
        [SetUp]
        public void Setup()
        {

        }

        [TestCase(42)]
        [TestCase(43)]
        public void TestMethod(int answer)
        {
            // TODO: Add your test code here
            Assert.That(answer, Is.EqualTo(42), "Some useful error message");
        }
        [TestCase(0)]
        [TestCase(1)]
        public void DivideByZeroException(int x)
        {
            //var ex = Assert.Catch<Exception>(() => defByZero());
            //StringAssert.Contains("нуль", ex.Message);
            Assert.That(() => defByZero(), Throws.TypeOf<DivideByZeroException>());

            System.Console.WriteLine(x);

            void defByZero()
            {
                var z = 100 / x;
            }
        }

        [Ignore("Просто игнор")]
        public void IgnoreTest()
        {

        }

        [Test]
        public void MoqTest()
        {

            var moq = new Mock<IRepository<List<string>>>();

            moq.Setup(x => x.GetListT()).Returns(new List<List<string>>());


            var mock = new Mock<IRepository2>();
            mock.Setup(a => a.GetListT()).Returns(new List<string>());

            Assert.IsTrue(true);
        }


        [TearDown]
        public void Dispose()
        {

        }

    }
}
