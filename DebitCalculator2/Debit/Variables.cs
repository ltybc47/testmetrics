﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DebitCalculator2.Debit
{
    public class Variable
    {
        /// <summary>
        /// Короткое имя, буквенное обозначение
        /// </summary>
        public string name;
        /// <summary>
        /// Полное название
        /// </summary>
        public string FullName;
        /// <summary>
        /// Размерность
        /// </summary>
        public string dimension;
        /// <summary>
        /// Значение
        /// </summary>
        public double value;
        public string description;
    }
    public static class Variables
    {
        public static Variable GetVariable(string Name, double val=0)
        {
            switch (Name)
            {
                case "G":
                    return new Variable() { name = "G" , description="Ускорение свободного падения", dimension="м/с", value = val};
                    break;
                case "Kv":
                    return new Variable() { name = "Kv" , description = "горизонтальная проницаемость пласта", dimension="мД", value = val };
                    break;
                case "Kh":
                    return new Variable() { name = "Kh" , description = "вертикальная проницаемость пласта", dimension="мД" , value = val};
                    break;
                case "a":
                    return new Variable() { name = "a" , description = "коэффициент анизотропии проницаемостей", dimension="-" , value = val};
                    break;
                case "h":
                    return new Variable() { name = "h" , description = "мощность пласта", dimension="м" , value = val};
                    break;
                case "Pпл":
                    return new Variable() { name = "Pпл", description = "давление пластовое", dimension="атм" , value = val};
                    break;
                case "Pзаб":
                    return new Variable() { name = "Pзаб", description = "давление забойное", dimension="атм" , value = val};
                    break;
                case "m":
                    return new Variable() { name = "m", description = "вязкость жидкости", dimension= "сП" , value = val};
                    break;
                case "bж":
                    return new Variable() { name = "bж", description = "Объемный коэффециент жидкости", dimension= "д.ед" , value = val};
                    break;
                case "re":
                    return new Variable() { name = "re", description = "радиус контура питания пласта", dimension= "м" , value = val};
                    break;
                case "rw":
                    return new Variable() { name = "rw", description = "радиус скважины", dimension= "м" , value = val};
                    break;
                case "fi":
                    return new Variable() { name = "fi", description = "угол отклонения от вертикали перфорированного участка ствола скважины", dimension= "рад" , value = val};
                    break;
                case "L":
                    return new Variable() { name = "L", description = "длина горизонтального участка ствола скважины", dimension= "рад" , value = val};
                    break;
                case "lg":
                    return new Variable() { name = "lg", value= 1, description = "ЧТО ЭТО??", dimension= "-" };
                    break;
                    ///
                    /// <КОНСТАНТЫ>
                    /// 
                case "Jd":
                    return new Variable() { name = "Jd", value= 0.177521412499014, description = "безразмерный коэффициент продуктивности", dimension= "-" };
                    break;
                    /// 
                    /// </КОНСТАНТЫ>
                    ///
            }
            throw new Exception($"dont now variable \" {Name} \"");
        }
    }
}
