﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DebitCalculator2.Debit
{
    /// <summary>
    /// Методики расчета псевдо-скинфактора скважин с наклонно-направленным стволом
    /// </summary>
    public class CincoMillerRameyCalc
    {
        private double fi, a, Ppl, Pzab, Kv, Kh, m, bzh, re, rw, lg, h, ln;
        /// <summary>
        /// Формула Псевдо-скинфактора
        /// </summary>
        public double S
        {
            get
            {
                _S = -Math.Pow(A / 41, 2.06) - Math.Pow(A / 56, 1.865) * lg * (hd / 100);
                return _S;
            }
        }
        /// <summary>
        /// Коэфециент для формуры псевдо-скинфактор
        /// </summary>
        public double A
        {
            get
            {
                _A = (180 / Math.PI) * Math.Atan(Math.Sqrt(a) * Math.Tan(fi));
                return _A;
            }
        }
        /// <summary>
        ///  дебит жидкости скважины 
        /// </summary>
        public double Qzh
        {
            get
            {
                _Qzh = Jd * (Kh * h * (Ppl - Pzab) / 18.41 * m * bzh);
                return _Qzh;
            }
        }
        /// <summary>
        /// эффективная мощность пласта
        /// </summary>
        public  double hd
        {
            get
            {
                _hd = (h / rw) * (1 / Math.Sqrt(a));
                return _hd;
            }
        }
        /// <summary>
        /// Безразмерный коэффициент продуктивности
        /// </summary>
        public double Jd
        {
            get
            {
                throw new Exception("неизвесна ln");
                _Jd = (1 / (ln * re / rw + S));
                return _Jd;
            }
        }
        private double _A, _S, _Qzh, _Jd, _hd;
        private double error;   //  введена для заглушки в формулах с неизвесными переменными
        public CincoMillerRameyCalc()
        {

        }
        /// <summary>
        /// считывания из общего массива данных переменных для этой формулы.
        /// </summary>
        /// <param name="variables"></param>
        public void AddListVariables(List<Variable> variables)
        {
            fi = variables.FirstOrDefault(x => x.name == "fi").value;
            a = variables.FirstOrDefault(x => x.name == "a").value;
            Ppl = variables.FirstOrDefault(x => x.name == "Pпл").value;
            Pzab = variables.FirstOrDefault(x => x.name == "Pзаб").value;
            Kv = variables.FirstOrDefault(x => x.name == "Kv").value;
            Kh = variables.FirstOrDefault(x => x.name == "Kh").value;
            m = variables.FirstOrDefault(x => x.name == "m").value;
            bzh = variables.FirstOrDefault(x => x.name == "bж").value;
            re = variables.FirstOrDefault(x => x.name == "re").value;
            rw = variables.FirstOrDefault(x => x.name == "rw").value;
            fi = variables.FirstOrDefault(x => x.name == "fi").value;
            h = variables.FirstOrDefault(x => x.name == "h").value;
        }


        /// <summary>
        /// коэффициент анизотропии проницаемостей
        /// </summary>
        /// <param name="kv">проницаемость пласта по вертикали</param>
        /// <param name="kh">горизонтальная проницаемость пласта</param>
        /// <returns></returns>
        private double GetCofAzintropii(double kv, double kh)
        {
            a = (kv / kh);
            return a;
        }

    }
    internal class Calc_Ozkan
    {

        private double error;   //  введена для заглушки в формулах с неизвесными переменными

        public Calc_Ozkan()
        {

        }
        private double GetS(double L)
        {
            double S;
            S = -error * error * (L / 1);
            return S;
        }
    }
}
